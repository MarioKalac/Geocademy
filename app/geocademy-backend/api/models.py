from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    school = models.CharField(max_length=255, blank=True, null=True)
    is_student = models.BooleanField(default=False)
    is_professor = models.BooleanField(default=False)
    students = models.ManyToManyField('self', through='ProfessorStudent', symmetrical=False)
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    class Meta:
        db_table = 'user'


class ProfessorStudent(models.Model):
    id = models.AutoField(primary_key=True)
    professor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='professor', on_delete=models.CASCADE,
                                  db_column='professor_id')
    student = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='student', on_delete=models.CASCADE,
                                db_column='student_id')

    class Meta:
        db_table = 'professor_student'


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'category'


class Type(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'type'


class Difficulty(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'difficulty'


class Question(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField()
    image = models.FileField(blank=True, null=True)
    categories = models.ManyToManyField(Category, through='QuestionCategory')
    # category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='category_id')
    type = models.ForeignKey(Type, on_delete=models.DO_NOTHING, db_column='type_id')
    difficulty = models.ForeignKey(Difficulty, on_delete=models.DO_NOTHING, db_column='difficulty_id')

    class Meta:
        db_table = 'question'


class QuestionCategory(models.Model):
    id = models.AutoField(primary_key=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, db_column='question_id')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='category_id')

    class Meta:
        db_table = 'question_category'


class Answer(models.Model):
    id = models.AutoField(primary_key=True)
    question = models.ForeignKey(Question, related_name='answers', on_delete=models.CASCADE, db_column='question_id')
    text = models.TextField()
    is_correct = models.BooleanField()

    class Meta:
        db_table = 'answer'


class StudentQuiz(models.Model):
    id = models.AutoField(primary_key=True)
    student = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, db_column='student_id')
    categories = models.ManyToManyField(Category, through='SelectedQuizCategory')
    types = models.ManyToManyField(Type, through='SelectedQuizType')
    difficulties = models.ManyToManyField(Difficulty, through='SelectedQuizDifficulty')
    time_started = models.DateTimeField()

    class Meta:
        db_table = 'student_quiz'


class SelectedQuizCategory(models.Model):
    id = models.AutoField(primary_key=True)
    quiz = models.ForeignKey(StudentQuiz, on_delete=models.CASCADE, db_column='student_quiz_id')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='selected_category_id')

    class Meta:
        db_table = 'selected_quiz_category'


class SelectedQuizType(models.Model):
    id = models.AutoField(primary_key=True)
    quiz = models.ForeignKey(StudentQuiz, on_delete=models.CASCADE, db_column='student_quiz_id')
    type = models.ForeignKey(Type, on_delete=models.CASCADE, db_column='selected_type_id')

    class Meta:
        db_table = 'selected_quiz_type'


class SelectedQuizDifficulty(models.Model):
    id = models.AutoField(primary_key=True)
    quiz = models.ForeignKey(StudentQuiz, on_delete=models.CASCADE, db_column='student_quiz_id')
    difficulty = models.ForeignKey(Difficulty, on_delete=models.CASCADE, db_column='selected_difficulty_id')

    class Meta:
        db_table = 'selected_quiz_difficulty'


class StudentQuizQuestionAnswer(models.Model):
    id = models.AutoField(primary_key=True)
    student_quiz = models.ForeignKey(StudentQuiz, on_delete=models.CASCADE, db_column='student_quiz_id')
    student_answer = models.ForeignKey(Answer, on_delete=models.CASCADE, db_column='student_answer_id')
    time_answered = models.DateTimeField()

    class Meta:
        db_table = 'student_quiz_question_answer'

# question = models.ForeignKey('Question', on_delete=models.CASCADE, db_column='question_id')
# correct_answer = models.ForeignKey('Answer', related_name='correct_answer', on_delete=models.CASCADE,
    #                                    db_column='correct_answer_id')

# class QuizQuestion(models.Model):
#     id = models.AutoField(primary_key=True)
#     quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE, db_column='quiz_id')
#     question = models.ForeignKey('Question', on_delete=models.CASCADE, db_column='question_id')
#     # quiz_question = models.ForeignKey('QuizQuestion', on_delete=models.CASCADE, db_column='quiz_question_id')
#
#     class Meta:
#         db_table = 'quiz_question'
