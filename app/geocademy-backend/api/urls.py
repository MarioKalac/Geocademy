from django.urls import path
from api.api import *

app_name = 'api'

urlpatterns = [
    path('categories', CategoriesAPI.as_view()),
    path('category/<int:id>', CategoryAPI.as_view()),
    path('types', TypeList.as_view()),
    path('difficulties', DifficultyList.as_view()),
    path('questions', QuestionAPI.as_view()),
    path('questions/<int:id>', QuestionAPI.as_view()),

    path('auth/register', RegistrationAPI.as_view()),
    path('auth/login', LoginAPI.as_view()),
    path('auth/user', UserAPI.as_view()),
]
