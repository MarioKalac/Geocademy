from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import *
import datetime


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('id', 'name')


class DifficultySerializer(serializers.ModelSerializer):
    class Meta:
        model = Difficulty
        fields = ('id', 'name')


class AnswerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False, required=False)

    class Meta:
        model = Answer
        fields = ('id', 'text', 'is_correct')


class QuestionSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True)
    type = TypeSerializer()
    difficulty = DifficultySerializer()
    answers = AnswerSerializer(many=True)

    class Meta:
        model = Question
        fields = ('id', 'text', 'image', 'type', 'difficulty', 'categories', 'answers')

    def create(self, validated_data):
        categories_data = validated_data.pop('categories')
        answers_data = validated_data.pop('answers')
        question = Question(
            text=validated_data['text'],
            image=validated_data['image'],
            type=Type.objects.get(name=validated_data.pop('type')['name']),
            difficulty=Difficulty.objects.get(name=validated_data.pop('difficulty')['name']),
        )
        question.save()
        for category_data in categories_data:
            question_category = QuestionCategory(
                question=question,
                category=Category.objects.get(name=category_data.get('name'))
            )
            question_category.save()
        for answer_data in answers_data:
            answer = Answer(
                question=question,
                text=answer_data.get('text'),
                is_correct=answer_data.get('is_correct')
            )
            answer.save()
        return question

    def update(self, instance, validated_data):
        categories_data = validated_data.pop('categories')
        answers_data = validated_data.pop('answers')
        instance.text = validated_data.get('text', instance.text)
        instance.image = validated_data.get('image', instance.image)
        instance.type = Type.objects.get(name=validated_data.pop('type').get('name', instance.type.name))
        instance.difficulty = Difficulty.objects.get(
            name=validated_data.pop('difficulty').get('name', instance.difficulty.name))
        instance.save()

        old_question_categories = QuestionCategory.objects.filter(question=instance)
        for old_question_category in old_question_categories:
            exists = False
            for category_data in categories_data:
                if old_question_category.category.name == category_data.get('name'):
                    exists = True
                    break
            if not exists:
                QuestionCategory.objects.get(id=old_question_category.id).delete()
        for category_data in categories_data:
            question_category, created = QuestionCategory.objects.get_or_create(
                question=instance,
                category=Category.objects.get(name=category_data.get('name'))
            )

        old_answers = Answer.objects.filter(question=instance)
        for old_answer in old_answers:
            exists = False
            for answer_data in answers_data:
                if old_answer.id == answer_data.get('id'):
                    exists = True
                    break
            if not exists:
                Answer.objects.get(id=old_answer.id).delete()
        for answer_data in answers_data:
            if answer_data.get('id') is None:
                answer_data['id'] = Answer.objects.last().id + 1
        for answer_data in answers_data:
            answer, created = Answer.objects.update_or_create(
                id=answer_data.get('id'),
                defaults={'id': answer_data.get('id'),
                          'question': instance,
                          'text': answer_data.get('text'),
                          'is_correct': answer_data.get('is_correct')}
            )
        return instance


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password', 'is_student', 'is_professor')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(first_name=validated_data['first_name'],
                                        last_name=validated_data['last_name'],
                                        email=validated_data['email'],
                                        username=validated_data['username'],
                                        password=validated_data['password'],
                                        is_student=validated_data['is_student'],
                                        is_professor=validated_data['is_professor'])
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'is_student', 'is_professor')


class LoginUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Unable to log in with provided credentials.")
