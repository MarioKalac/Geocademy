from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie


class Login(View):
    @method_decorator(ensure_csrf_cookie)
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            return redirect('/')


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect('')
