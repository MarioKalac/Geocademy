import React, {Component} from 'react';
import '../styles/home.css';
import '../styles/form-submit-button.css'


class Home extends Component {
    render() {
        return (
            <div className="container" id="home">
                <div className="row justify-content-center">
                    <div className="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-5">
                        <a className="btn btn-success btn-block" role="button" id="submitButton" href="/">Nastavi kao gost</a>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-5">
                        <a className="btn btn-success btn-block" role="button" id="submitButton" href="/prijava">Prijavi se</a>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-xl-3 col-lg-3 col-md-4 col-sm-5 col-5">
                        <a className="btn btn-success btn-block" role="button" id="submitButton" href="/registracija">Registriraj se</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;