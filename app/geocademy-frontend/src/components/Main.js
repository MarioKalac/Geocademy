import React, {Component} from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from "./Home";
import Login from './Login';
import Register from './Register';
import Quiz from './Quiz';
import Statistics from './Statistics';
import Categories from './Categories';
import Questions from './Questions';
import Students from './Students';
import Student from './Student';


class Main extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' render={() => <Home />} />
                    <Route exact path='/prijava' render={() => <Login />} />
                    <Route exact path='/registracija' render={() => <Register />} />
                    <Route exact path='/kviz' render={() => <Quiz />} />
                    <Route exact path='/statistika' render={() => <Statistics />} />
                    <Route exact path='/kategorije' render={() => <Categories />} />
                    <Route exact path='/pitanja' render={() => <Questions />} />
                    <Route exact path='/ucenici' render={() => <Students />} />
                    <Route path='/ucenici/:number' render={() => <Student />} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Main;