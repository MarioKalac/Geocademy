import React, {Component} from 'react';
import {connect} from 'react-redux'
import {auth} from '../actions';
import '../styles/register.css';
import '../styles/form-submit-button.css'

class Register extends Component {
    state = {
        first_name: '',
        last_name: '',
        email: '',
        username: '',
        password: '',
        is_student: '',
        is_professor: ''
        // password2: ''
    };

    onSubmit = event => {
        event.preventDefault();
        this.props.register(this.state.first_name, this.state.last_name, this.state.email, this.state.username,
                            this.state.password, this.state.is_student, this.state.is_professor);
    };

    onClickRegular = () => {
        document.getElementById('selectProfessor').style.display = 'none';
        this.setState({is_student: false, is_professor: false});
    };

    onClickStudent = () => {
        document.getElementById('selectProfessor').style.display = 'block';
        this.setState({is_student: true, is_professor: false});
    };

    onClickProfessor = () => {
        document.getElementById('selectProfessor').style.display = 'none';
        this.setState({is_student: false, is_professor: true});
    };

    render() {
        if (this.props.isAuthenticated) {

        }
            return (
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-6 col-lg-7 col-md-8 col-sm-10 col-10">
                            <form id="registerForm" onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <h6>Tip korisnika</h6>
                                    <div className="form-check form-check-inline col-4">
                                        <input type="radio" className="form-check-input" id="user_regular" name="user_type"
                                               onClick={this.onClickRegular} />
                                        <label className="form-check-label" htmlFor="user_regular">Obični korisnik</label>
                                    </div>
                                    <div className="form-check form-check-inline col-4">
                                        <input type="radio" className="form-check-input" id="user_student" name="user_type"
                                               onClick={this.onClickStudent} />
                                        <label className="form-check-label" htmlFor="user_student">Student</label>
                                    </div>
                                    <div className="form-check form-check-inline col-3">
                                        <input type="radio" className="form-check-input" id="user_professor" name="user_type"
                                               onClick={this.onClickProfessor} />
                                        <label className="form-check-label" htmlFor="user_professor">Profesor</label>
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-6">
                                        <label htmlFor="first_name">Ime</label>
                                        <input type="text" className="form-control" id="first_name" placeholder="Upišite ime"
                                               onChange={event => this.setState({first_name: event.target.value})} />
                                    </div>
                                    <div className="form-group col-6">
                                        <label htmlFor="lastName">Prezime</label>
                                        <input type="text" className="form-control" id="lastName" placeholder="Upišite prezime"
                                               onChange={event => this.setState({last_name: event.target.value})} />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email adresa</label>
                                    <input type="email" className="form-control" id="email" placeholder="Upišite email adresu"
                                           onChange={event => this.setState({email: event.target.value})} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="username">Korisničko ime</label>
                                    <input type="text" className="form-control" id="username" placeholder="Upišite korisničko ime"
                                           onChange={event => this.setState({username: event.target.value})} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Lozinka</label>
                                    <input type="password" className="form-control" id="password" placeholder="Upišite lozinku"
                                           onChange={event => this.setState({password: event.target.value})} />
                                </div>
                                <div className="form-group" id="selectProfessor">
                                    <label htmlFor="professor">Profesor</label>
                                    <select className="form-control" id="professor">
                                        <option>Odaberi svog profesora</option>
                                    </select>
                                </div>
                                {/*<div className="form-group">*/}
                                    {/*<input type="password" className="form-control" id="password2"*/}
                                           {/*placeholder="Ponovno upišite lozinku" onChange={event => this.setState({password2: event.target.value})} />*/}
                                {/*</div>*/}
                                <div className="text-center">
                                    <button type="submit" className="btn btn-success" id="submitButton">Registriraj se</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
    }
}

const mapStateToProps = state => {
    let errors = [];
    if (state.auth.errors) {
        errors = Object.keys(state.auth.errors).map(field => {
            return {field, message: state.auth.errors[field]};
        });
    }
    return {
        errors,
        isAuthenticated: state.auth.isAuthenticated
    };
};

const mapDispatchToProps = dispatch => {
    return {
        register: (first_name, last_name, email, username, password, is_student, is_professor) => {
            return dispatch(auth.register(first_name, last_name, email, username, password, is_student, is_professor));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);