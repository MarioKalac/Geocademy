import React, {Component} from 'react';
import NavBrand from './NavBrand';
import NavItem from './NavItem';
import NavDropdown from './NavDropdown';

class NavBar extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand">
                <NavBrand brandLink="/" brandText="Geocademy" />
                <ul className="navbar-nav">
                    {this.props.navItems.map((navItem) => (
                        <NavItem key={navItem.navItemText}
                                 navItemLink={navItem.navItemLink}
                                 navItemText={navItem.navItemText} />
                    ))}
                </ul>
                <ul className="navbar-nav ml-auto">
                    <NavDropdown adminName={this.props.adminName}
                                 dropdownItems={this.props.dropdownItems} />
                </ul>
            </nav>
        );
    }
}

export default NavBar;