import React, {Component} from 'react';
import {connect} from 'react-redux';
import {categories} from '../actions';
import TableHeader from './TableHeader';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPlus from "@fortawesome/fontawesome-free-solid/faPlus";
import faEdit from "@fortawesome/fontawesome-free-regular/faEdit";
import faTrash from "@fortawesome/fontawesome-free-regular/faTrashAlt";
import '../styles/add-button.css';
import '../styles/table-buttons.css';
import '../styles/form-submit-button.css';

class Categories extends Component {
    state = {
        name: '',
        updateCategoryId: null
    };

    componentDidMount() {
        this.props.getCategories();
    }

    submitCategory = event => {
        event.preventDefault();
        if (this.state.updateCategoryId === null) {
            this.props.addCategory(this.state.name).then(this.setState({name: '', updateCategoryId: null}));
        } else {
            this.props.updateCategory(this.state.updateCategoryId, this.state.name)
                .then(this.setState({name: '', updateCategoryId: null}));
        }
    };

    selectCategoryForUpdate = (id) => {
        let category = this.props.categories.find(category => category.id === id);
        this.setState({name: category.name, updateCategoryId: id});
    };

    render() {
        return (
            <div className="container-fluid" id="categories">
                <div className="row">
                    <div className="col-xl-2 col-lg-2 col-md-2">
                        <button type="button" className="btn btn-outline-success" id="addButton" data-toggle="modal"
                                data-target="#addCategoryModal"><FontAwesomeIcon icon={faPlus} size="lg" /> Dodaj kategoriju
                        </button>

                        <div className="modal fade" id="addCategoryModal">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header justify-content-center">
                                        <h3 className="modal-title">Dodavanje kategorije</h3>
                                    </div>
                                    <div className="modal-body">
                                        <form onSubmit={this.submitCategory}>
                                            <div className="form-group">
                                                <label htmlFor="categoryName">Naziv kategorije</label>
                                                <input type="text" className="form-control" id="categoryName"
                                                       placeholder="Upišite naziv kategorije" value={this.state.name}
                                                       onChange={event => this.setState({name: event.target.value})} required />
                                            </div>
                                            <div className="text-center">
                                                <button type="submit" className="btn btn-success" id="submitButton">Dodaj kategoriju</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="modal-footer justify-content-center">
                                        <button type="button" className="btn btn-danger" id="deleteButton" data-dismiss="modal">Odustani</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="col-xl-10 col-lg-10 col-md-10">
                        <table className="table table-sm table-bordered table-striped table-hover">
                            <TableHeader columnNames={["#", "Naziv", "Uredi", "Obriši"]} />
                            <tbody>
                                {this.props.categories.map(category => (
                                    <tr key={category.id}>
                                        <td>{category.id}</td>
                                        <td>{category.name}</td>
                                        <td>
                                            <button className="btn btn-sm btn-edit" title="Uredi" data-toggle="modal"
                                                    data-target="#editCategoryModal" onClick={() => this.selectCategoryForUpdate(category.id)}>
                                                <FontAwesomeIcon icon={faEdit} size="2x" />
                                            </button>

                                            <div className="modal fade" id="editCategoryModal">
                                                <div className="modal-dialog">
                                                    <div className="modal-content">
                                                        <div className="modal-header justify-content-center">
                                                            <h3 className="modal-title">Uređivanje kategorije</h3>
                                                        </div>
                                                        <div className="modal-body">
                                                            <form onSubmit={this.submitCategory}>
                                                                <div className="form-group">
                                                                    <label htmlFor="categoryName">Naziv kategorije</label>
                                                                    <input type="text" className="form-control" id="categoryName"
                                                                           value={this.state.name}
                                                                           onChange={event => this.setState({name: event.target.value})} required />
                                                                </div>
                                                                <div className="text-center">
                                                                    <button type="submit" className="btn btn-success" id="submitButton">Uredi kategoriju</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="modal-footer justify-content-center">
                                                            <button type="button" className="btn btn-danger" id="deleteButton" data-dismiss="modal">Odustani</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <button className="btn btn-sm btn-delete" title="Obriši" data-toggle="modal"
                                                    data-target={"#deleteCategoryModal" + category.id}>
                                                <FontAwesomeIcon icon={faTrash} size="2x" />
                                            </button>

                                            <div className="modal fade" id={"deleteCategoryModal" + category.id}>
                                                <div className="modal-dialog">
                                                    <div className="modal-content">
                                                        <div className="modal-header justify-content-center">
                                                            <h3 className="modal-title">Brisanje kategorije</h3>
                                                        </div>
                                                        <div className="modal-body">
                                                            <h5>Jeste li sigurni da želite obrisati kategoriju <b>{category.name}</b>?</h5>
                                                            <div className="text-center">
                                                                <button type="submit" className="btn btn-danger" id="deleteButton"
                                                                        onClick={() => this.props.deleteCategory(category.id)}>Obriši kategoriju
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="modal-footer justify-content-center">
                                                            <button type="button" className="btn btn-outline-success" id="addButton" data-dismiss="modal">Odustani</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        categories: state.categories
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getCategories: () => {
            dispatch(categories.getCategories());
        },
        addCategory: (name) => {
            return dispatch(categories.addCategory(name));
        },
        updateCategory: (id, name) => {
            return dispatch(categories.updateCategory(id, name));
        },
        deleteCategory: (id) => {
            dispatch(categories.deleteCategory(id));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);