import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {auth} from '../actions';
import CSRFToken from './CSRFToken';
import '../styles/login.css';
import '../styles/form-submit-button.css'

class Login extends Component {
    state = {
        username: '',
        password: ''
    };

    onSubmit = event => {
        event.preventDefault();
        this.props.login(this.state.username, this.state.password);
    };

    render() {
        if (this.props.isAuthenticated && !this.props.user.is_professor) {
            console.log(this.props.user);
            return <Redirect to="/kviz" />
        } else if (this.props.isAuthenticated && this.props.user.is_professor) {
            console.log(this.props.user);
            return <Redirect to="/pitanja" />
        }
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6 col-lg-7 col-md-8 col-sm-10 col-10">
                        <form id="loginForm" onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label htmlFor="username">Korisničko ime</label>
                                <input type="text" className="form-control" id="username" placeholder="Upišite korisničko ime"
                                       onChange={event => this.setState({username: event.target.value})} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Lozinka</label>
                                <input type="password" className="form-control" id="password" placeholder="Upišite lozinku"
                                       onChange={event => this.setState({password: event.target.value})} />
                            </div>
                            <div className="text-center">
                                <button type="submit" className="btn btn-success" id="submitButton">Prijavi se</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    let errors = [];
    if (state.auth.errors) {
        errors = Object.keys(state.auth.errors).map(field => {
            return {field, message: state.auth.errors[field]};
        });
    }
    return {
        errors,
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        login: (username, password) => {
            return dispatch(auth.login(username, password));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);