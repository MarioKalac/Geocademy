import React, {Component} from 'react';
import TableHeader from './TableHeader';
import StudentsTableRow from './StudentsTableRow';
import '../styles/categories.css';

class Categories extends Component {
    render() {
        return (
            <div className="container-fluid" id="students">
                <div className="row">
                    <div className="col-xl-2 col-lg-2 col-md-2">

                    </div>

                    <div className="col-xl-10 col-lg-10 col-md-10">
                        <table className="table table-bordered table-striped table-hover">
                            <TableHeader columnNames={["#", "Ime", "Prezime"]} />
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Categories;