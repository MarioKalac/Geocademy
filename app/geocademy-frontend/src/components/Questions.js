import React, {Component} from 'react';
import {connect} from 'react-redux';
import {questions, categories} from '../actions';
import TableHeader from './TableHeader';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPlus from "@fortawesome/fontawesome-free-solid/faPlus";
import faEdit from "@fortawesome/fontawesome-free-regular/faEdit";
import faTrash from "@fortawesome/fontawesome-free-regular/faTrashAlt";
import '../styles/questions.css';
import jQuery from 'jquery';
let $ = jQuery;

class Questions extends Component {
    state = {
        text: '',
        image: null,
        type: {name: ''},
        difficulty: {name: ''},
        categories: [],
        answers: [{text: '', is_correct: null}, {text: '', is_correct: null}, {text: '', is_correct: null},
                  {text: '', is_correct: null}, {text: '', is_correct: null}],
        updateQuestionId: null
    };

    componentDidMount() {
        this.props.getQuestions();
        this.props.getCategories();
    }

    submitQuestion = event => {
        event.preventDefault();
        let categories = [];
        let answers = [];
        for (let i = 0; i < this.state.categories.length; i++) {
            categories.push({name: this.state.categories[i]});
        }
        for (let i = 0; i < 5; i++) {
            if (this.state.answers[i].text !== '' && this.state.answers[i].is_correct !== null) {
                answers.push(this.state.answers[i]);
            }
        }
        if (this.state.updateQuestionId === null) {
            this.props.addQuestion(this.state.text, this.state.image, this.state.type, this.state.difficulty, categories, answers)
                .then(this.setState({text: '', image: null, type: {name: ''}, difficulty: {name: ''}, categories: [],
                                    answers: [{text: '', is_correct: null}, {text: '', is_correct: null}, {text: '', is_correct: null},
                                    {text: '', is_correct: null}, {text: '', is_correct: null}], updateQuestionId: null}));
        } else {
            this.props.updateQuestion(this.state.updateQuestionId, this.state.text, this.state.image, this.state.type, this.state.difficulty, categories, answers)
                .then(this.setState({text: '', image: null, type: {name: ''}, difficulty: {name: ''}, categories: [], updateQuestionId: null}));
        }
    };

    selectQuestionForUpdate = id => {
        let question = this.props.questions.find(question => question.id === id);
        let categories = [];
        let answers = [];
        for (let i = 0; i < question.categories.length; i++) {
            categories.push(question.categories[i].name);
        }
        for (let i = 0; i < question.answers.length; i++) {
            answers.push({id: question.answers[i].id, text: question.answers[i].text, is_correct: question.answers[i].is_correct})
        }
        let numberOfAnswers = answers.length;
        while (numberOfAnswers < 5) {
            answers.push({id: null, text: '', is_correct: null});
            numberOfAnswers++;
        }
        this.setState({text: question.text, image: question.image, type: {name: question.type.name},
                       difficulty: {name: question.difficulty.name}, categories: categories, answers: answers,
                       updateQuestionId: id});
    };

    showDataOnEditModal = () => {
        $('.selectpicker').selectpicker('render');
    };

    addAnswer = () => {
        console.log(this.state);
    };

    render() {
        return (
            <div className="container-fluid" id="questions">
                <div className="row">
                    <div className="col-xl-2 col-lg-2 col-md-2">
                        <button type="button" className="btn btn-outline-success" id="addButton" data-toggle="modal"
                                data-target="#addQuestionModal"><FontAwesomeIcon icon={faPlus} size="lg" /> Dodaj pitanje
                        </button>

                        <div className="modal fade" id="addQuestionModal">
                            <div className="modal-dialog modal-lg">
                                <div className="modal-content">
                                    <div className="modal-header justify-content-center">
                                        <h3 className="modal-title">Dodavanje pitanja</h3>
                                    </div>
                                    <div className="modal-body">
                                        <form id="addQuestionForm" onSubmit={this.submitQuestion}>
                                            <div className="form-group">
                                                <label htmlFor="questionText">Tekst pitanja</label>
                                                <textarea className="form-control" id="questionText"
                                                       placeholder="Upišite tekst pitanja" value={this.state.text}
                                                       onChange={event => this.setState({text: event.target.value})} required />
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-6">
                                                    <select className="selectpicker form-control" data-width="fit" data-style=""
                                                            title="Odaberite tip pitanja" value={this.state.type.name}
                                                            onChange={event => this.setState({type: {name: event.target.value}})}>
                                                        <option value="Tekstualna pitanja">Tekstualna pitanja</option>
                                                        <option value="Pojam na karti">Pojam na karti</option>
                                                        <option value="Povezivanje srodnih pojmova">Povezivanje srodnih pojmova</option>
                                                    </select>
                                                </div>
                                                <div className="form-group col-6">
                                                    <select className="selectpicker form-control" data-width="fit" data-style=""
                                                            title="Odaberite težinu pitanja" value={this.state.difficulty.name}
                                                            onChange={event => this.setState({difficulty: {name: event.target.value}})}>
                                                        <option value="Vrlo lako">Vrlo lako</option>
                                                        <option value="Lako">Lako</option>
                                                        <option value="Srednje">Srednje</option>
                                                        <option value="Teško">Teško</option>
                                                        <option value="Vrlo teško">Vrlo teško</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <select className="selectpicker" multiple data-live-search="true"
                                                        data-width="fit" data-style="" title="Odaberite kategoriju/e" value={this.state.categories}
                                                        onChange={event => this.setState({categories: [...event.target.selectedOptions].map(category => category.value)})}>
                                                    {this.props.categories.map(category => (
                                                        <option key={category.id} value={category.name}>{category.name}</option>
                                                    ))}
                                                </select>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-9">
                                                    <label htmlFor="answerText">Tekst odgovora</label>
                                                    <textarea className="form-control" id="answerText"
                                                           placeholder="Upišite tekst odgovora" value={this.state.answers[0] !== undefined && this.state.answers[0].text}
                                                           onChange={event => this.setState({answers: {...this.state.answers, 0: {text: event.target.value, is_correct: this.state.answers[0].is_correct}}})} required />
                                                </div>
                                                <div className="form-group col-3">
                                                    <label htmlFor="answerCorrect">Točnost odgovora</label>
                                                    <select className="selectpicker form-control" id="answerCorrect" data-width="fit" data-style="" title="Odaberite točnost" required
                                                            onChange={event => this.setState({answers: {...this.state.answers, 0: {text: this.state.answers[0].text, is_correct: event.target.value === 'true'}}})}>
                                                        <option value="true">Da</option>
                                                        <option value="false">Ne</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-9">
                                                    <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[1] !== undefined && this.state.answers[1].text}
                                                              onChange={event => this.setState({answers: {...this.state.answers, 1: {text: event.target.value, is_correct: this.state.answers[1].is_correct}}})} />
                                                </div>
                                                <div className="form-group col-3">
                                                    <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                            onChange={event => this.setState({answers: {...this.state.answers, 1: {text: this.state.answers[1].text, is_correct: event.target.value === 'true'}}})}>
                                                        <option value="true">Da</option>
                                                        <option value="false">Ne</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-9">
                                                    <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[2] !== undefined && this.state.answers[2].text}
                                                              onChange={event => this.setState({answers: {...this.state.answers, 2: {text: event.target.value, is_correct: this.state.answers[2].is_correct}}})} />
                                                </div>
                                                <div className="form-group col-3">
                                                    <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                            onChange={event => this.setState({answers: {...this.state.answers, 2: {text: this.state.answers[2].text, is_correct: event.target.value === 'true'}}})}>
                                                        <option value="true">Da</option>
                                                        <option value="false">Ne</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-9">
                                                    <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[3] !== undefined && this.state.answers[3].text}
                                                              onChange={event => this.setState({answers: {...this.state.answers, 3: {text: event.target.value, is_correct: this.state.answers[3].is_correct}}})} />
                                                </div>
                                                <div className="form-group col-3">
                                                    <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                            onChange={event => this.setState({answers: {...this.state.answers, 3: {text: this.state.answers[3].text, is_correct: event.target.value === 'true'}}})}>
                                                        <option value="true">Da</option>
                                                        <option value="false">Ne</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-row">
                                                <div className="form-group col-9">
                                                    <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[4] !== undefined && this.state.answers[4].text}
                                                              onChange={event => this.setState({answers: {...this.state.answers, 4: {text: event.target.value, is_correct: this.state.answers[4].is_correct}}})} />
                                                </div>
                                                <div className="form-group col-3">
                                                    <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                            onChange={event => this.setState({answers: {...this.state.answers, 4: {text: this.state.answers[4].text, is_correct: event.target.value === 'true'}}})}>
                                                        <option value="true">Da</option>
                                                        <option value="false">Ne</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="addAnswer">
                                                <button type="button" className="btn btn-outline-success" id="addButton" onClick={this.addAnswer}>
                                                    <FontAwesomeIcon icon={faPlus} size="lg" /> Dodaj odgovor
                                                </button>
                                            </div>
                                            <div className="text-center">
                                                <button type="submit" className="btn btn-success" id="submitButton">Dodaj pitanje</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="modal-footer justify-content-center">
                                        <button type="button" className="btn btn-danger" id="deleteButton" data-dismiss="modal">Odustani</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12">
                        <table className="table table-sm table-bordered table-striped table-hover">
                            <TableHeader columnNames={["#", "Tekst pitanja", "Tip pitanja", "Težina", "Kategorija", "Uredi", "Obriši"]} />
                            <tbody>
                                {this.props.questions.map(question => (
                                    <tr key={question.id}>
                                        <td>{question.id}</td>
                                        <td>{question.text}</td>
                                        <td>{question.type.name}</td>
                                        <td>{question.difficulty.name}</td>
                                        <td>{question.categories.map((category, i) => {
                                            return question.categories.length !== i + 1 ?
                                                <span key={category.id}>{category.name}, </span> :
                                                <span key={category.id}>{category.name}</span>
                                        })}
                                        </td>
                                        <td>
                                            <button className="btn btn-sm btn-edit" title="Uredi" data-toggle="modal"
                                                    data-target="#editQuestionModal" onClick={() => this.selectQuestionForUpdate(question.id)}>
                                                <FontAwesomeIcon icon={faEdit} size="2x" />
                                            </button>

                                            <div className="modal fade" id="editQuestionModal" onTransitionEnd={this.showDataOnEditModal}>
                                                <div className="modal-dialog modal-lg">
                                                    <div className="modal-content">
                                                        <div className="modal-header justify-content-center">
                                                            <h3 className="modal-title">Uređivanje pitanja</h3>
                                                        </div>
                                                        <div className="modal-body">
                                                            <form id="editQuestionForm" onSubmit={this.submitQuestion}>
                                                                <div className="form-group">
                                                                    <label htmlFor="questionText">Tekst pitanja</label>
                                                                    <textarea className="form-control" id="questionText"
                                                                              placeholder="Upišite tekst pitanja" value={this.state.text}
                                                                              onChange={event => this.setState({text: event.target.value})} required />
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-6">
                                                                        <select className="selectpicker form-control type-select" data-width="fit" data-style=""
                                                                                title="Odaberite tip pitanja" value={this.state.type.name}
                                                                                onChange={event => this.setState({type: {name: event.target.value}})}>
                                                                            <option value="Tekstualna pitanja">Tekstualna pitanja</option>
                                                                            <option value="Pojam na karti">Pojam na karti</option>
                                                                            <option value="Povezivanje srodnih pojmova">Povezivanje srodnih pojmova</option>
                                                                        </select>
                                                                    </div>
                                                                    <div className="form-group col-6">
                                                                        <select className="selectpicker form-control difficulty-select" data-width="fit" data-style=""
                                                                                title="Odaberite težinu pitanja" value={this.state.difficulty.name}
                                                                                onChange={event => this.setState({difficulty: {name: event.target.value}})}>
                                                                            <option value="Vrlo lako">Vrlo lako</option>
                                                                            <option value="Lako">Lako</option>
                                                                            <option value="Srednje">Srednje</option>
                                                                            <option value="Teško">Teško</option>
                                                                            <option value="Vrlo teško">Vrlo teško</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group">
                                                                    <select className="selectpicker form-control categories-select" multiple data-live-search="true"
                                                                            data-width="fit" data-style="" title="Odaberite kategoriju/e" value={this.state.categories}
                                                                            onChange={event => this.setState({categories: [...event.target.selectedOptions].map(category => category.value)})}>
                                                                        {this.props.categories.map(category => (
                                                                            <option key={category.id} value={category.name}>{category.name}</option>
                                                                        ))}
                                                                    </select>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-9">
                                                                        <label htmlFor="answerText">Tekst odgovora</label>
                                                                        <textarea className="form-control" id="answerText"
                                                                                  placeholder="Upišite tekst odgovora" value={this.state.answers[0] !== undefined ? this.state.answers[0].text : ''}
                                                                                  onChange={event => this.setState({answers: {...this.state.answers, 0: {id: this.state.answers[0].id, text: event.target.value, is_correct: this.state.answers[0].is_correct}}})} required />
                                                                    </div>
                                                                    <div className="form-group col-3">
                                                                        <label htmlFor="answerCorrect">Točnost odgovora</label>
                                                                        <select className="selectpicker form-control" id="answerCorrect" data-width="fit" data-style="" title="Odaberite točnost" required
                                                                                value={(this.state.answers[0] !== undefined) && this.state.answers[0].is_correct !== null ? this.state.answers[0].is_correct : ''}
                                                                                onChange={event => this.setState({answers: {...this.state.answers, 0: {id: this.state.answers[0].id, text: this.state.answers[0].text, is_correct: event.target.value === 'true'}}})}>
                                                                            <option value="true">Da</option>
                                                                            <option value="false">Ne</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-9">
                                                                        <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[1] !== undefined ? this.state.answers[1].text : ''}
                                                                                  onChange={event => this.setState({answers: {...this.state.answers, 1: {id: this.state.answers[1].id, text: event.target.value, is_correct: this.state.answers[1].is_correct}}})} />
                                                                    </div>
                                                                    <div className="form-group col-3">
                                                                        <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                                                value={(this.state.answers[1] !== undefined) && this.state.answers[1].is_correct !== null ? this.state.answers[1].is_correct : ''}
                                                                                onChange={event => this.setState({answers: {...this.state.answers, 1: {id: this.state.answers[1].id, text: this.state.answers[1].text, is_correct: event.target.value === 'true'}}})}>
                                                                            <option value="true">Da</option>
                                                                            <option value="false">Ne</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-9">
                                                                        <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[2] !== undefined ? this.state.answers[2].text : ''}
                                                                                  onChange={event => this.setState({answers: {...this.state.answers, 2: {id: this.state.answers[2].id, text: event.target.value, is_correct: this.state.answers[2].is_correct}}})} />
                                                                    </div>
                                                                    <div className="form-group col-3">
                                                                        <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                                                value={(this.state.answers[2] !== undefined) && this.state.answers[2].is_correct !== null ? this.state.answers[2].is_correct : ''}
                                                                                onChange={event => this.setState({answers: {...this.state.answers, 2: {id: this.state.answers[2].id, text: this.state.answers[2].text, is_correct: event.target.value === 'true'}}})}>
                                                                            <option value="true">Da</option>
                                                                            <option value="false">Ne</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-9">
                                                                        <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[3] !== undefined ? this.state.answers[3].text : ''}
                                                                                  onChange={event => this.setState({answers: {...this.state.answers, 3: {id: this.state.answers[3].id, text: event.target.value, is_correct: this.state.answers[3].is_correct}}})} />
                                                                    </div>
                                                                    <div className="form-group col-3">
                                                                        <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                                                value={(this.state.answers[3] !== undefined) && this.state.answers[3].is_correct !== null ? this.state.answers[3].is_correct : ''}
                                                                                onChange={event => this.setState({answers: {...this.state.answers, 3: {id: this.state.answers[3].id, text: this.state.answers[3].text, is_correct: event.target.value === 'true'}}})}>
                                                                            <option value="true">Da</option>
                                                                            <option value="false">Ne</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="form-group col-9">
                                                                        <textarea className="form-control" placeholder="Upišite tekst odgovora" value={this.state.answers[4] !== undefined ? this.state.answers[4].text : ''}
                                                                                  onChange={event => this.setState({answers: {...this.state.answers, 4: {id: this.state.answers[4].id, text: event.target.value, is_correct: this.state.answers[4].is_correct}}})} />
                                                                    </div>
                                                                    <div className="form-group col-3">
                                                                        <select className="selectpicker form-control" data-width="fit" data-style="" title="Odaberite točnost"
                                                                                value={(this.state.answers[4] !== undefined) && this.state.answers[4].is_correct !== null ? this.state.answers[4].is_correct : ''}
                                                                                onChange={event => this.setState({answers: {...this.state.answers, 4: {id: this.state.answers[4].id, text: this.state.answers[4].text, is_correct: event.target.value === 'true'}}})}>
                                                                            <option value="true">Da</option>
                                                                            <option value="false">Ne</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div id="addAnswer">
                                                                    <button type="button" className="btn btn-outline-success" id="addButton" onClick={this.addAnswer}>
                                                                        <FontAwesomeIcon icon={faPlus} size="lg" /> Dodaj odgovor
                                                                    </button>
                                                                </div>
                                                                <div className="text-center">
                                                                    <button type="submit" className="btn btn-success" id="submitButton">Dodaj pitanje</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="modal-footer justify-content-center">
                                                            <button type="button" className="btn btn-danger" id="deleteButton" data-dismiss="modal">Odustani</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <button className="btn btn-sm btn-delete" title="Obriši" data-toggle="modal"
                                                    data-target={"#deleteQuestionModal" + question.id}>
                                                <FontAwesomeIcon icon={faTrash} size="2x" />
                                            </button>

                                            <div className="modal fade" id={"deleteQuestionModal" + question.id}>
                                                <div className="modal-dialog">
                                                    <div className="modal-content">
                                                        <div className="modal-header justify-content-center">
                                                            <h3 className="modal-title">Brisanje pitanja</h3>
                                                        </div>
                                                        <div className="modal-body">
                                                            <h5>Jeste li sigurni da želite obrisati pitanje <b>{question.text}</b>?</h5>
                                                            <div className="text-center">
                                                                <button type="submit" className="btn btn-danger" id="deleteButton"
                                                                        onClick={() => this.props.deleteQuestion(question.id)}>Obriši pitanje
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div className="modal-footer justify-content-center">
                                                            <button type="button" className="btn btn-outline-success" id="addButton" data-dismiss="modal">Odustani</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        questions: state.questions,
        categories: state.categories
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getCategories: () => {
            dispatch(categories.getCategories());
        },
        getQuestions: () => {
            dispatch(questions.getQuestions());
        },
         addQuestion: (text, image, type, difficulty, categories, answers) => {
            console.log(answers);
            return dispatch(questions.addQuestion(text, image, type, difficulty, categories, answers));
         },
        updateQuestion: (id, text, image, type, difficulty, categories, answers) => {
            return dispatch(questions.updateQuestion(id, text, image, type, difficulty, categories, answers));
        },
        deleteQuestion: (id) => {
            dispatch(questions.deleteQuestion(id));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Questions);