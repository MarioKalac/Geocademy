import React, {Component} from 'react';
import '../styles/quiz.css';

class Quiz extends Component {
    render() {
        return (
            <div className="container-fluid" id="quiz">
                <div className="row justify-content-center">
                    <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                        <h5>Kategorije: kategorija1, kategorija2</h5>
                        <h5>Težina: srednje</h5>
                        <button className="btn btn-outline-primary">Završi</button>
                    </div>
                    <div className="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 text-center">
                        <img src="http://via.placeholder.com/500x300?text=Slika+pitanja+ako+postoji" />
                        <p>Ovo je primjer teksta nekog pitanja?</p>
                        <div className="row">
                            <div className="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 ml-auto">
                                <button className="btn btn-outline-primary">Odgovor 1</button>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 mr-auto">
                                <button className="btn btn-outline-primary">Odgovor 2</button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 ml-auto">
                                <button className="btn btn-outline-primary">Odgovor 3</button>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 mr-auto">
                                <button className="btn btn-outline-primary">Odgovor 4</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Quiz;
