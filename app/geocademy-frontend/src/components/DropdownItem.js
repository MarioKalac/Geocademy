import React, {Component} from 'react';

class DropdownItem extends Component {
    render() {
        return (
            <a className="dropdown-item" href={this.props.dropdownItemLink}>{this.props.dropdownItemText}</a>
        );
    }
}

export default DropdownItem;