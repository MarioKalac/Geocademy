import React, {Component} from 'react';
import TableButton from './TableButton';
import faEdit from '@fortawesome/fontawesome-free-regular/faEdit';
import faTrash from '@fortawesome/fontawesome-free-regular/faTrashAlt';

class QuestionsTableRow extends Component {
    render() {
        return (
            <tr>

            </tr>
        );
    }
}

export default QuestionsTableRow;

// addAnswer1 = () => {
//     let addQuestionForm = document.getElementById('addQuestionForm');
//     let addAnswer = document.getElementById('addAnswer');
//
//     let formRow = document.createElement('div');
//     formRow.classList.add('form-row');
//     let answerTextFormGroup = document.createElement('div');
//     answerTextFormGroup.classList.add('form-group', 'col-9');
//     let answerText = document.createElement('textarea');
//     answerText.classList.add('form-control');
//     answerText.setAttribute('placeholder', 'Upišite tekst odgovora');
//
//
//     let answerCorrectFormGroup = document.createElement('div');
//     answerCorrectFormGroup.classList.add('form-group', 'col-3');
//
//     let answerCorrectDropdown = document.createElement('div');
//     answerCorrectDropdown.classList.add('dropdown', 'bootstrap-select', 'form-control', 'fit-width');
//
//     let answerCorrectSelect = document.createElement('select');
//     answerCorrectSelect.classList.add('selectpicker', 'form-control');
//     answerCorrectSelect.setAttribute('data-width', 'fit');
//     answerCorrectSelect.setAttribute('data-style', '');
//     answerCorrectSelect.setAttribute('title', 'Odaberite točnost');
//     answerCorrectSelect.setAttribute('tabindex', '-98');
//
//     let answerCorrectButton = document.createElement('button');
//     answerCorrectButton.classList.add('btn', 'dropdown-toggle', 'bs-placeholder');
//     answerCorrectButton.setAttribute('type', 'button');
//     answerCorrectButton.setAttribute('data-toggle', 'dropdown');
//     answerCorrectButton.setAttribute('role', 'button');
//     answerCorrectButton.setAttribute('title', 'Odaberite točnost');
//
//     let answerCorrectDropdownMenu = document.createElement('div');
//     answerCorrectDropdownMenu.classList.add('dropdown-menu');
//     answerCorrectButton.setAttribute('role', 'combobox');
//
//     let answerCorrectTitle = document.createElement('option');
//     let answerCorrectYes = document.createElement('option');
//     let answerCorrectNo = document.createElement('option');
//     answerCorrectTitle.classList.add('bs-title-option');
//     answerCorrectYes.innerHTML = 'Da';
//     answerCorrectNo.innerHTML = 'Ne';
//
//     let answerCorrectButtonFilterOption = document.createElement('div');
//     answerCorrectButtonFilterOption.classList.add('filter-option');
//     let answerCorrectButtonFilterOptionInner = document.createElement('div');
//     answerCorrectButtonFilterOptionInner.classList.add('filter-option-inner');
//     let answerCorrectButtonFilterOptionInnerInner = document.createElement('div');
//     answerCorrectButtonFilterOptionInnerInner.classList.add('filter-option-inner-inner');
//     answerCorrectButtonFilterOptionInnerInner.innerHTML = 'Odaberite točnost';
//
//     let answerCorrectDropdownMenuInner = document.createElement('div');
//     answerCorrectDropdownMenuInner.classList.add('inner', 'show');
//     answerCorrectDropdownMenuInner.setAttribute('role', 'listbox');
//     answerCorrectDropdownMenuInner.setAttribute('tabindex', '-1');
//     let answerCorrectDropdownMenuUl = document.createElement('ul');
//     answerCorrectDropdownMenuUl.classList.add('dropdown-menu', 'inner', 'show');
//     let answerCorrectDropdownMenuUlLi1 = document.createElement('li');
//     let answerCorrectDropdownMenuUlLi2 = document.createElement('li');
//     let answerCorrectDropdownMenuUlLiA1 = document.createElement('a');
//     let answerCorrectDropdownMenuUlLiA2 = document.createElement('a');
//     answerCorrectDropdownMenuUlLiA1.classList.add('dropdown-item');
//     answerCorrectDropdownMenuUlLiA1.setAttribute('role', 'option');
//     answerCorrectDropdownMenuUlLiA1.setAttribute('tabindex', '0');
//     answerCorrectDropdownMenuUlLiA2.classList.add('dropdown-item');
//     answerCorrectDropdownMenuUlLiA2.setAttribute('role', 'option');
//     answerCorrectDropdownMenuUlLiA2.setAttribute('tabindex', '0');
//     let answerCorrectDropdownMenuUlLiASpan1 = document.createElement('span');
//     let answerCorrectDropdownMenuUlLiASpan2 = document.createElement('span');
//     answerCorrectDropdownMenuUlLiASpan1.classList.add('text');
//     answerCorrectDropdownMenuUlLiASpan1.innerHTML = 'Da';
//     answerCorrectDropdownMenuUlLiASpan2.classList.add('text');
//     answerCorrectDropdownMenuUlLiASpan2.innerHTML = 'Ne';
//
//     formRow.appendChild(answerTextFormGroup);
//     formRow.appendChild(answerCorrectFormGroup);
//     answerTextFormGroup.appendChild(answerText);
//
//     answerCorrectFormGroup.appendChild(answerCorrectDropdown);
//     answerCorrectDropdown.appendChild(answerCorrectSelect);
//     answerCorrectDropdown.appendChild(answerCorrectButton);
//     answerCorrectDropdown.appendChild(answerCorrectDropdownMenu);
//
//     answerCorrectButton.appendChild(answerCorrectButtonFilterOption);
//     answerCorrectButtonFilterOption.appendChild(answerCorrectButtonFilterOptionInner);
//     answerCorrectButtonFilterOptionInner.appendChild(answerCorrectButtonFilterOptionInnerInner);
//
//     answerCorrectSelect.appendChild(answerCorrectTitle);
//     answerCorrectSelect.appendChild(answerCorrectYes);
//     answerCorrectSelect.appendChild(answerCorrectNo);
//
//     answerCorrectDropdownMenu.appendChild(answerCorrectDropdownMenuInner);
//     answerCorrectDropdownMenuInner.appendChild(answerCorrectDropdownMenuUl);
//     answerCorrectDropdownMenuUl.appendChild(answerCorrectDropdownMenuUlLi1);
//     answerCorrectDropdownMenuUl.appendChild(answerCorrectDropdownMenuUlLi2);
//     answerCorrectDropdownMenuUlLi1.appendChild(answerCorrectDropdownMenuUlLiA1);
//     answerCorrectDropdownMenuUlLi2.appendChild(answerCorrectDropdownMenuUlLiA2);
//     answerCorrectDropdownMenuUlLiA1.appendChild(answerCorrectDropdownMenuUlLiASpan1);
//     answerCorrectDropdownMenuUlLiA2.appendChild(answerCorrectDropdownMenuUlLiASpan2);
//
//     addQuestionForm.insertBefore(formRow, addAnswer);
// };