import React, {Component} from 'react';

class NavItem extends Component {
    render() {
        return (
            <li className="nav-item">
                <a className="nav-link" href={this.props.navItemLink}>{this.props.navItemText}</a>
            </li>
        );
    }
}

export default NavItem;