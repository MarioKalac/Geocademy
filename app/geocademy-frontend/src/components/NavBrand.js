import React, {Component} from 'react';

class NavBrand extends Component {
    render() {
        return (
            <a className="navbar-brand" href={this.props.brandLink}>{this.props.brandText}</a>
        );
    }
}

export default NavBrand;
