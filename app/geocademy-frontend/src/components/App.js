import React, {Component} from 'react';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import AppReducer from '../reducers'
import NavBar from './NavBar';
import Main from "./Main";

let store = createStore(AppReducer, applyMiddleware(thunk));

class App extends Component {
    render() {
        // if () {
        //     // navbar za studenta
        // } else if () {
        //     // navbar za prof
        // }
        return (
            <Provider store={store}>
                <div>
                    <NavBar navItems={[{"navItemText":"Kviz", "navItemLink":"/kviz"},
                                        {"navItemText":"Statistika", "navItemLink":"/statistika"},
                                        {"navItemText":"Kategorije", "navItemLink":"/kategorije"},
                                        {"navItemText":"Pitanja", "navItemLink":"/pitanja"},
                                        {"navItemText":"Učenici", "navItemLink":"/ucenici"}]}
                            dropdownItems={[{"dropdownItemText":"Uredi osobne podatke", "dropdownItemLink":"#"},
                                            {"dropdownItemText":"Promijeni lozinku", "dropdownItemLink":"#"},
                                            {"dropdownItemText":"Odjavi se", "dropdownItemLink":"#"}]}
                            adminName="Ime Prezime" />
                    <Main loginFormFieldItems={[{"formFieldId":"loginUsername", "formFieldType":"text", "formFieldName":"username", "formFieldLabel":"Korisničko ime", "formFieldPlaceholder":"Upišite korisničko ime"},
                                                {"formFieldId":"loginPassword", "formFieldType":"password", "formFieldName":"password", "formFieldLabel":"Lozinka", "formFieldPlaceholder":"Upišite lozinku"}]} />
                </div>
            </Provider>
        );
    }
}

export default App;
