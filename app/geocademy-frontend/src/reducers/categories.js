const initialState = [];

export default function categories(state=initialState, action) {
    let categories = state.slice();

    switch (action.type) {
        case 'GET_CATEGORIES':
            return [...state, ...action.categories];

        case 'ADD_CATEGORY':
            return [...state, action.category];

        case 'UPDATE_CATEGORY':
            let categoryToUpdate = categories.find(category => category.id === action.id);
            categoryToUpdate.name = action.category.name;
            categories.splice(action.id-1, 1, categoryToUpdate);
            return categories;

        case 'DELETE_CATEGORY':
            categories.splice(action.id-1, 1);
            return categories;

        default:
            return state;
    }
}
