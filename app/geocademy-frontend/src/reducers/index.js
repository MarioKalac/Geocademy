import {combineReducers} from 'redux';
import auth from './auth';
import questions from './questions';
import categories from './categories';

const AppReducer = combineReducers({
    auth, questions, categories,
});

export default AppReducer;