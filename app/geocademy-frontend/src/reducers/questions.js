const initialState = [];

export default function questions(state=initialState, action) {
    let questions = state.slice();

    switch (action.type) {
        case 'GET_QUESTIONS':
            return [...state, ...action.questions];

        case 'ADD_QUESTION':
            return [...state, action.question];

        case 'UPDATE_QUESTION':
            let questionToUpdate = questions.find(question => question.id === action.id);
            questionToUpdate.text = action.question.text;
            questionToUpdate.image = action.question.image;
            questionToUpdate.type = action.question.type;
            questionToUpdate.difficulty = action.question.difficulty;
            questionToUpdate.categories = action.question.categories;
            questionToUpdate.answers = action.question.answers;
            questions.splice(action.id-1, 1, questionToUpdate);
            return questions;

        case 'DELETE_QUESTION':
            questions.splice(action.id-1, 1);
            return questions;

        default:
            return state;
    }
}