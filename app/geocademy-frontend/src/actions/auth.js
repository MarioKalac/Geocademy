export const register = (first_name, last_name, email, username, password, is_student, is_professor) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({first_name, last_name, email, username, password, is_student, is_professor});
        console.log(body);

        return fetch('/api/auth/register', {headers, body, method: 'POST'})
            .then(response => {
                if (response.status < 500) {
                    return response.json().then(data => {
                        return {status: response.status, data};
                    })
                } else {
                    console.log("Server error!");
                    throw response;
                }
            })
            .then(response => {
                if (response.status === 200) {
                    dispatch({type: 'REGISTRATION_SUCCESSFUL', data: response.data});
                    return response.data;
                } else if (response.status === 401 || response.status === 403) {
                    dispatch({type: 'AUTHENTICATION_ERROR', data: response.data});
                    throw response.data;
                } else {
                    dispatch({type: 'REGISTRATION_FAILED', data: response.data});
                    throw response.data;
                }
            })
    }
};

export const login = (username, password) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({username, password});

        return fetch('/api/auth/login', {headers, body, method: 'POST'})
            .then(response => {
                if (response.status < 500) {
                    return response.json().then(data => {
                        return {status: response.status, data};
                    })
                } else {
                    console.log("Server error!");
                    throw response;
                }
            })
            .then(response => {
                if (response.status === 200) {
                    dispatch({type: 'LOGIN_SUCCESSFUL', data: response.data});
                    return response.data;
                } else if (response.status === 401 || response.status === 403) {
                    dispatch({type: 'AUTHENTICATION_ERROR', data: response.data});
                    throw response.data;
                } else {
                    dispatch({type: 'LOGIN_FAILED', data: response.data});
                    throw response.data;
                }
            })
    }
};

export const logout = () => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};

        return fetch('/api/auth/logout', {headers, body: '', method: 'POST'})
            .then(response => {
                if (response.status === 204) {
                    return {status: response.status, data: {}};
                } else if (response.status < 500) {
                    return response.json().then(data => {
                        return {status: response.status, data};
                    })
                } else {
                    console.log("Server error!");
                    throw response;
                }
            })
            .then(response => {
                if (response.status === 204) {
                    dispatch({type: 'LOGOUT_SUCCESSFUL'});
                    return response.data;
                } else if (response.status === 401 || response.status === 403) {
                    dispatch({type: 'AUTHENTICATION_ERROR', data: response.data});
                    throw response.data;
                }
            })
    }
};

export const loadUser = () => {
    return (dispatch, getState) => {
        dispatch({type: 'USER_LOADING'});
        const token = getState().auth.token;
        let headers = {"Content-Type": "application/json"};
        if (token) {
            headers['Authorization'] = 'Token ' + token;
        }

        return fetch('/api/auth/user', {headers, method: 'GET'})
            .then(response => {
                if (response.status < 500) {
                    return response.json().then(data => {
                        return {status: response.status, data};
                    })
                } else {
                    console.log("Server error!");
                    throw response;
                }
            })
            .then(response => {
                if (response.status === 200) {
                    dispatch({type: 'USER_LOADED', user: response.data});
                    return response.data;
                } else if (response.status >= 400 && response < 500) {
                    dispatch({type: 'AUTHENTICATION_ERROR', data:response.data});
                    throw response.data;
                }
            })
    }
};