export const getCategories = () => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};

        return fetch('/api/categories', {headers, method: 'GET'})
            .then(response => response.json())
            .then(categories => {
                return dispatch({
                    type: 'GET_CATEGORIES',
                    categories
                })
            })
    }
};

export const addCategory = name => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({name});

        return fetch('/api/categories', {headers, method: 'POST', body})
            .then(response => response.json())
            .then(category => {
                return dispatch({
                    type: 'ADD_CATEGORY',
                    category
                })
            })
    }
};

export const updateCategory = (id, name) => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({name});

        return fetch('/api/category/' + id, {headers, method: 'PUT', body})
            .then(response => response.json())
            .then(category => {
                return dispatch({
                    type: 'UPDATE_CATEGORY',
                    category,
                    id
                })
            })
    }
};

export const deleteCategory = id => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};

        return fetch('/api/category/' + id, {headers, method: 'DELETE'})
            .then(response => {
                if (response.ok) {
                    return dispatch({
                        type: 'DELETE_CATEGORY',
                        id
                    })
                }
            })
    }
};
