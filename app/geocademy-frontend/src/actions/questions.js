export const getQuestions = () => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};

        return fetch('/api/questions', {headers, method: 'GET'})
            .then(response => response.json())
            .then(questions => {
                return dispatch({
                    type: 'GET_QUESTIONS',
                    questions
                })
            })
    }
};

export const addQuestion = (text, image, type, difficulty, categories, answers) => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({text, image, type, difficulty, categories, answers});

        return fetch('/api/questions', {headers, method: 'POST', body})
            .then(response => response.json())
            .then(question => {
                return dispatch({
                    type: 'ADD_QUESTION',
                    question
                })
            })
    }
};

export const updateQuestion = (id, text, image, type, difficulty, categories, answers) => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};
        let body = JSON.stringify({text, image, type, difficulty, categories, answers});

        return fetch('/api/questions/' + id, {headers, method: 'PUT', body})
            .then(response => response.json())
            .then(question => {
                return dispatch({
                    type: 'UPDATE_QUESTION',
                    question,
                    id
                })
            })
    }
};

export const deleteQuestion = id => {
    return dispatch => {
        let headers = {"Content-Type": "application/json"};

        return fetch('/api/question/' + id, {headers, method: 'DELETE'})
            .then(response => {
                if (response.ok) {
                    return dispatch({
                        type: 'DELETE_QUESTION',
                        id
                    })
                }
            })
    }
};
