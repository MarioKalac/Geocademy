import * as auth from './auth';
import * as questions from './questions';
import * as categories from './categories';

export {auth, questions, categories}